FROM python:3.9-slim

RUN pip install flask

COPY app.py .

ENV FLASK_APP=app.py


CMD ["flask", "run", "--host=0.0.0.0"]
